import pytest

from v2_dataset.orm import Column, StrictForeignKey, declarative_base, types


@pytest.fixture()
def fix_Model():
    Model = declarative_base()
    return Model


@pytest.fixture()
def fix_Entity1(fix_Model):
    class Entity1(fix_Model):
        a = Column(types.Integer, primary_key=True)
        b = Column(types.String)

    return Entity1


@pytest.fixture
def fix_entity1(fix_Entity1, fix_entity1_params):
    instance = fix_Entity1(**fix_entity1_params)
    return instance


@pytest.fixture(params=[dict(a=1, b="Hey")])
def fix_entity1_params(request):
    return request.param


class Test_Entity1:
    def test_CLASS_ATTRIBUTE_DUNDER_tablename(self, fix_Entity1):
        assert fix_Entity1.__tablename__ == "Entity1"

    def test_CLASS_ATTRIBUTE_model_classes(self, fix_Model, fix_Entity1):
        assert (
            fix_Entity1.model_classes
            == fix_Model.model_classes
            == {"Entity1": fix_Entity1}
        )

    def test_CLASS_METHOD_columns(self, fix_Entity1):
        assert fix_Entity1.columns() == {"a": fix_Entity1.a, "b": fix_Entity1.b}

    def test_METHOD_DUNDER_eq(self, fix_Entity1, fix_entity1, fix_entity1_params):
        entity1_clone = fix_Entity1(**fix_entity1_params)
        entity1_diff = fix_Entity1(**fix_entity1_params)
        entity1_diff.a += 1
        assert entity1_clone == fix_entity1
        assert entity1_diff != fix_entity1

    def test_METHOD_DUNDER_hash(self, fix_entity1):
        answer = hash((fix_entity1.a, fix_entity1.b))
        result = hash(fix_entity1)
        assert result == answer

    def test_METHOD_DUNDER_repr(self, fix_entity1):
        answer = f"{fix_entity1.__class__.__name__}(a={fix_entity1.a!r}, b={fix_entity1.b!r})"
        result = repr(fix_entity1)
        assert result == answer

    def test_METHOD_compare(self, fix_Entity1, fix_entity1, fix_entity1_params):
        entity1_clone = fix_Entity1(**fix_entity1_params)
        entity1_diff = fix_Entity1(**fix_entity1_params)
        entity1_diff.a += 1
        assert entity1_clone.compare(fix_entity1)
        assert not entity1_diff.compare(fix_entity1)

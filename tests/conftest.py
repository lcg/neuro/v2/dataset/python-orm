import pytest

from click.testing import CliRunner
from pathlib import Path
from v2_dataset import __main__, db, model

#: Repository's 'tests/' directory.
_tests_data_dir = Path(__file__).parent / "data"
_tests_recording_ids = [51, 229]
_tests_plx_paths = [_tests_data_dir / f"{id}/file.plx" for id in _tests_recording_ids]


@pytest.fixture(scope="session")
def cli_db(tmp_path_factory):
    """SQLite database on temporary file, initialized with curated information and after using the CLI to parse all test
    recordings.

    :rtype: v2_dataset.db.Database"""
    db_path = tmp_path_factory.mktemp("cli_db") / "index.sqlite"
    database = db.Database(conn_string=f"sqlite:///{db_path!s}")
    database.init(curated_dir=None)
    runner = CliRunner()
    for rec_id, plx_path in zip(_tests_recording_ids, _tests_plx_paths):
        args = f"--db {db_path!s} recording parse --id {rec_id} --plx-path {plx_path!s}"
        result = runner.invoke(__main__.main, args.split(), catch_exceptions=False)
        assert result.exit_code == 0
    return database


@pytest.fixture(scope="session")
def ref_db():
    """Read-only SQLite database for reference comparisons. Includes parsing of all test recordings.

    :rtype: v2_dataset.db.Database"""
    ref_index_path = _tests_data_dir / "index.sqlite"
    database = db.Database(f"sqlite:///{ref_index_path!s}")
    yield database
    database.scoped_session.rollback()


@pytest.fixture(scope="session", params=_tests_recording_ids)
def ref_recording(request, ref_db):
    """One of the test recordings included, bound to the reference database (:func:`ref_db`).

    :rtype: v2_dataset.model.Recording"""
    rec_id = request.param
    rec = ref_db.query(model.Recording).filter_by(id=rec_id).one()
    return rec


@pytest.fixture(params=[229])
def ref_recording_grating105(request, ref_db):
    rec_id = request.param
    rec = ref_db.query(model.Recording).filter_by(id=rec_id).one()
    return rec


@pytest.fixture(params=[51])
def ref_recording_mb8(request, ref_db):
    rec_id = request.param
    rec = ref_db.query(model.Recording).filter_by(id=rec_id).one()
    return rec

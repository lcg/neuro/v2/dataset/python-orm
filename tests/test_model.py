import pytest

from v2_dataset import model, units


class Test_Model:
    entity_classes = model.Model.model_classes
    non_parsed_entity_classes = {
        name: cls
        for name, cls in entity_classes.items()
        if name
        not in [
            "SpikeChannel",
            "Trial",
            "SortedChannel",
            "SortedUnit",
            "Sorting",
            "EmbeddedSorting",
            "OtherSorting",
        ]
    }

    @pytest.mark.parametrize(
        "entity_class",
        non_parsed_entity_classes.values(),
        ids=list(non_parsed_entity_classes.keys()),
    )
    def test_SCENARIO_databases_contents_match(self, entity_class, ref_db, cli_db):
        ref_entities = ref_db.query(entity_class).all()
        tmp_entities = cli_db.query(entity_class).all()
        assert tmp_entities == ref_entities

    def test_CLASS_ATTRIBUTE_model_classes(self):
        result = model.Model.model_classes
        answer = {
            "Subject": model.Subject,
            "RecordingSession": model.RecordingSession,
            "Electrode": model.Electrode,
            "Recording": model.Recording,
            "RecordingPath": model.RecordingPath,
            "SpikeChannel": model.SpikeChannel,
            "Trial": model.Trial,
            "Sorting": model.Sorting,
            "OtherSorting": model.OtherSorting,
            "SortedChannel": model.SortedChannel,
            "SortedUnit": model.SortedUnit,
            "EmbeddedSorting": model.EmbeddedSorting,
            "MEA": model.MEA,
            "Protocol": model.Protocol,
            "Gratings": model.Gratings,
            "MovingBars": model.MovingBars,
            "Condition": model.Condition,
            "GratingCondition": model.GratingCondition,
            "MovingBarCondition": model.MovingBarCondition,
        }
        assert result == answer


class Test_Protocol:
    def test_MB(self, ref_db, ref_recording_mb8):
        session = ref_db.session()
        protocol = ref_recording_mb8.protocol
        conditions = list(
            session.query(model.MovingBarCondition)
            .filter_by(protocol_name=protocol.name)
            .all()
        )

        assert protocol.conditions == conditions
        assert len(conditions) == 12

        assert protocol.no_conditions() == 12
        assert protocol.no_conditions(stimulus_only=False) == 12
        assert protocol.no_conditions(stimulus_only=True) == 8

        assert protocol.condition_time == 3000 * units.ms
        assert protocol.pre_trial_time == 0 * units.ms
        assert protocol.post_trial_time == 0 * units.ms
        assert protocol.total_trial_time == 3000 * units.ms

    def test_GRATING(self, ref_db, ref_recording_grating105):
        session = ref_db.session()
        protocol = ref_recording_grating105.protocol
        conditions = list(
            session.query(model.GratingCondition)
            .filter_by(protocol_name=protocol.name)
            .all()
        )

        assert protocol.conditions == conditions
        assert len(conditions) == 109

        assert protocol.no_conditions() == 109
        assert protocol.no_conditions(stimulus_only=False) == 109
        assert protocol.no_conditions(stimulus_only=True) == 105

        assert protocol.condition_time == 2000 * units.ms
        assert protocol.pre_trial_time == 300 * units.ms
        assert protocol.post_trial_time == 200 * units.ms
        assert protocol.total_trial_time == 2500 * units.ms


class Test_Trial:
    @pytest.fixture()
    def regular_trial(self, ref_db):
        trial = (
            ref_db.query(model.Trial).filter(model.Trial.condition_code < 250).first()
        )
        return trial

    def test_PROPERTIES_mid_trial_AND_post_trial_AND_pre_trial(self, regular_trial):
        pre_trial = regular_trial.pre_trial
        mid_trial = regular_trial.mid_trial
        post_trial = regular_trial.post_trial
        protocol = regular_trial.condition.protocol

        # Pre/mid/post-trial phases are tightly packed in time.
        assert pre_trial.start_ts == regular_trial.start_ts
        assert pre_trial.end_ts == mid_trial.start_ts
        assert mid_trial.end_ts == post_trial.start_ts
        assert post_trial.end_ts == regular_trial.end_ts

        # Pre/mid/post-trial are ordered correctly in time.
        assert pre_trial.start_ts <= mid_trial.start_ts < post_trial.start_ts
        assert pre_trial.end_ts < mid_trial.end_ts <= post_trial.end_ts

        # Pre/mid/post-trial durations are consistent with the protocol, given the trial's sampling rate.
        assert pre_trial.duration_ts == int(
            protocol.pre_trial_time * regular_trial.sampling_rate
        )
        assert (
            mid_trial.duration_ts
            == int(protocol.condition_time * regular_trial.sampling_rate)
            + regular_trial.excess_ts
        )
        assert post_trial.duration_ts == int(
            protocol.post_trial_time * regular_trial.sampling_rate
        )

        # More consistency properties involving the durations.
        assert (
            pre_trial.duration_ts + mid_trial.duration_ts + post_trial.duration_ts
            == regular_trial.duration_ts
        )
        assert mid_trial.duration_ts > regular_trial.excess_ts

        for t in [pre_trial, mid_trial, post_trial]:
            assert t.end_ts - t.start_ts == t.duration_ts


class Test_Recording:
    def test_PROPERTY_channels(self, ref_recording):
        assert ref_recording.channels == 64

    def test_SCENARIO_complementary_sorting_relationships(self, ref_recording):
        if ref_recording.embedded_sorting is None:
            answer = ref_recording.other_sortings
        else:
            answer = [ref_recording.embedded_sorting] + ref_recording.other_sortings
        assert ref_recording.sortings == answer

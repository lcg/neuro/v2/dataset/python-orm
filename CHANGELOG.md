# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].

## [Unreleased]
### Added

* `v2_dataset.model.Trial` has now properties `excess_ts`, `excess_tim`, `pre_trial`, `mid_trial`, and `post_trial` that describe specifics of condition presentation.  
* `v2_dataset.model.protocol.Gratings.as_array()` has a `color` option for specifying target colorspace and datatype.

### Changed

* `v2_dataset.model.core.SpikeChannel.waveforms` property renamed to `waveforms_ad` and `v2_dataset.model.core.SpikeChannel.waveforms` is now a cached property that converts A/D samples back to analog values in milliVolts.
  Mixins were also fixed to reflect this.
* `v2_dataset.parsing._parse_trials()` no longer requires a `v2_dataset.model.protocol.Protocol` parameter, as it just returns a Numpy array with condition codes, not condition objects.
  Actual condition object binding is done by the `v2_dataset.parsing.parse_trials()` function, which takes a `v2_dataset.model.Recording` object and hence knows how to determine the correct condition object from the condition codes.
* CLI `v2_dataset.__main__.recording_parse` now 
    * Opens a new session for every parsed file, to make debugging easier when SQL exceptions are raised, and
    * Inserts new condition codes into the database

### Removed

* Uniqueness constraint involving non-primary key fields of `v2_dataset.model.protocol.Protocol` entity type lifted.
  **Reason:** protocols like `Gratings 73` and `Gratings 105` have identical fields, and their difference is in the amount of associated `v2_dataset.model.protocol.Condition` objects (entities in another table), which cannot be coded as a SQL uniqueness constraint &mdash; the actual logical constraint (quite complicate) is that two protocol entities cannot coexist if all of their attributes (except name) are the same and they are associated to the same set of conditions or to two disjoint ordered sets of conditions which are themselves identical.

## [0.6.3] &ndash; 2020-03-24

* Restricted least compatible [dataset version](https://gitlab.com/lcg/neuro/v2/dataset/contents/tags/0.4.2) to 0.4.2 due to curation fixes.

## [0.6.2] &ndash; 2020-03-17
### Changed

* Lifted uniqueness contraint in Protocol class that is not enforceable in most recent compatible [dataset version](https://gitlab.com/lcg/neuro/v2/dataset/contents/tags/0.4.1).

### Fixed

* Regenerated [test database](tests/data/index.sqlite) to match most recent compatible [dataset version](https://gitlab.com/lcg/neuro/v2/dataset/contents/tags/0.4.1).

## [0.6.1] &ndash; 2019-11-15
### Changed

* Fixed protocol-to-condition array conversion method. 

## [0.6.0] &ndash; 2019-11-15
### Added

* Entity types for protocols, conditions, and specializations thereof.
* Tests for `v2_dataset.orm`.
* Attributes for condition, pre-/post-trial, and total trial duration in stimulus enumerations.

### Changed

* Replaced `v2_dataset.model.Stimulus` by `v2_dataset.model.Protocol` in relationships.
* Click exceptions are now channeled through the tests (`click.testing.CliRunner.invoke` with `catch_exceptions=False`), so if an exception is reaised while executing a tested command, it will be easier to identify the source of the error in the backtrace. 
* `v2_dataset.model.Model.columns()` method now returns an ordered dictionary that maps column names to column descriptors.
* Fixed condition 73 in grating stimulus (spontaneous activity), which was incorrectly specified.

## [0.5.0] &mdash; 2019-11-06
### Added

* All CLI commands can now specify target database file and whether to perform auto-initialization using the `--db` and `--init/-i` switches.
* `v2_dataset.db.Database` can now be instantiated by passing a path to a SQLite file.

### Changed

* `python -m v2_dataset --db <path> init` now really initializes the database (and fails if it has already been initialized, unless the `--overwrite/-w` switch is passed).
* `v2_dataset.model.GratingConditions` and `v2_dataset.model.MovingBarConditions` considerably enriched and moved to `v2_dataset.model.stimulus` module.
* Not using `tqdm.autonotebook` for database manipulation routines anymore, as it can crash in some terminals.
* `v2_dataset.db.Database` instantiation by passing a SQLAlchemy connection string is deprecated.

## [0.4.0] &mdash; 2019-10-31
### Added

* `v2_dataset.model.base.auto_cached_property` class decorator for all classes that employ *mixins* from `v2_dataset.mixins`, making them automatically invalidate properties when an attribute is set.

### Changed

* Fixed a bug where database entities based on *mixin* classes from `v2_dataset.mixins` could not have cached properties implemented as a combination of `@property` and `@functools.lru_cache` (since SQLAlchemy database entities are mutable, hence unhashable).
  Solution:
  * All *mixin* classes in `v2_dataset.mixins` that were using `functools.lru_cache` now use `cached_property.cached_property`.
  * All classes in `v2_dataset.model` that use *mixins* from `v2_dataset.mixins` now employ the `v2_dataset.model.base.auto_cached_property` class decorator. 
* Fixed `v2_dataset.model.Recording.channels` property (and added a test for it).

### Removed

* `dataset/tables/*.csv` files no longer included in source distribution, since the [contents repository] is no longer a subtree.

## [0.3.0] &mdash; 2019-10-14
### Added

* The `v2_dataset.compatible_dataset_version_range` module-level object now specifies the compatible versions of the [contents repository].
  If an incompatible version is found, a `RuntimeError` exception is raised.
* ORM-support for multi-electrode arrays (MEA), including:
  * New `v2_dataset.model.MEA` class.
  * Relationships between `v2_dataset.model.MEA` and `v2_dataset.model.RecordingSession` classes.
* New convenience hybrid properties and properties for `RecordingSession`:
  * `start_datetime`,
  * `end_datetime`, and
  * `duration`.
* Command `python -m v2_dataset parse` has a new `--use-exported` flag to allow using a pre-exported PLX file.

### Changed

* Package moved from TestPYPI to PYPI.
* The V2 Dataset's [contents repository] is no longer included as a subtree, because its Git Annex symlinks missed the actual `.git` directory by one level, which would require some subtree + annex magic to get things working.
  Now, the [contents repository] should be symlinked from the working directory or pointed by an environment var, as explained in the [README].
* Fixed `v2_dataset.model.core.Recording.path` property.
* Fixed a bug that made `v2_dataset.model.sorting.EmbeddedSorting.sorted_units` property unusable.
  
### Removed

* Redundant columns removed from `RecordingSession` model class:
  * ``electrode_rows``
  * ``electrode_cols``
  * ``electrode_left``
  * ``electrode_right``
  * ``start_datetime``
  * ``end_datetime``

## [0.2.2] &mdash; 2019-10-07
### Changed

* Bugs
  * The `V2_DATASET_DIR` setting was mistakenly tied to the `V2__DATASET_DIR` environment variable.

## [0.2.1] &mdash; 2019-07-29
### Changed

* Fixed dependencies 
  * `lcg-neuro-compneuro` library from version 0.2.0 to 0.1.0, and
  * `lcg-neuro-plx` library from version 0.2.1 to 0.2.0.

## [0.2.0] &mdash; 2019-07-29
### Added

* Command-line interface
  * New `v2_dataset init` command.

## [0.1.0] &mdash; 2019-07-25
### Added

* [`.bumpversion.cfg`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/.bumpversion.cfg) &mdash; Configuration file for the [bumpversion] version-tagging package.
* [`.coveragerc`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/.coveragerc) &mdash; Configuration file for the [Coverage] reporting tool.
* [`.gitignore`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/.gitignore) &mdash; List of files and directories paths/patterns [ignored by Git][gitignore].
* [`.gitlab-ci.yml`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/.gitlab-ci.yml) &mdash; Continuous integration/deploy configuration ([GitLab CI]), configured for:
  * Running a [pytest]-based test suite and reporting results with [Codecov] at https://codecov.io/gl/lcg:neuro:v2:dataset/python-orm,
  * Building the [Sphinx]-based documentation and deploying it via [Gitlab Pages] to https://lcg.gitlab.io/neuro/v2/dataset/python-orm, and
  * Uploading successfully built, tagged distributions to [TestPyPI] (defaults to https://test.pypi.org/project/lcg-neuro-v2-dataset).
* [`.pre-commit-config.yaml`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/.pre-commit-config.yaml) &mdash; Configuration file for the [pre-commit] package, which aids in applying useful [Git Hooks] in team workflows. Includes:
  * Automatic code styling with [Black].
* [`CHANGELOG.md`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/CHANGELOG.md) &mdash; this very history file, which follows the [Keep a Changelog] standard.
* [`MANIFEST.md`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/MANIFEST.md) Repository's manifest.
* [`README.md`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/README.md) &mdash; repository front-page.
* [`dataset`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/dataset) &mdash; the [contents repository], merged in as a subtree.
* [`docs/`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/docs) &mdash; [Sphinx]-based documentation setup, which includes:
  * [`conf.py`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/docs/conf.py) &mdash; [Sphinx] configuration file,
  * [`docs/index.rst`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/docs/index.rst) &mdash; documentation master file, and
  * [`Makefile`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/docs/Makefile) for building the docs easily.
* [`poetry.lock`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/poetry.lock) &mdash; [Poetry]'s resolved dependency file. Whereas [`pyproject.toml`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/pyproject.toml) specifies [*as abstract as possible*][post-setup-vs-requirements] dependencies, this file 
* [`pyproject.toml`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/pyproject.toml) &mdash; [PEP-517]-compliant packaging metadata, configured with the [Poetry] system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic [setup.py] file found in *classical* Python packaging.
* [`src/v2_dataset`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/v2_dataset) &mdash; Base directory of the example Python package distributed by this repository.
* [`tests`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/0.1.0/tests) &mdash; [pytest]-powered test-suite.

[LICENSE]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/LICENSE.txt
[MANIFEST]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/MANIFEST.md
[README]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/README.md
[0.6.3]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/compare?from=0.6.2&to=0.6.3
[0.6.2]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/compare?from=0.6.1&to=0.6.2
[0.6.1]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/compare?from=0.6.0&to=0.6.1
[0.6.0]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/compare?from=0.5.0&to=0.6.0
[0.5.0]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/compare?from=0.4.0&to=0.5.0
[0.4.0]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/compare?from=0.3.0&to=0.4.0
[0.3.0]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/compare?from=0.2.2&to=0.3.0
[0.2.2]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/compare?from=0.2.1&to=0.2.2
[0.2.1]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/compare?from=0.2.0&to=0.2.1
[0.2.0]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/compare?from=0.1.0&to=0.2.0
[0.1.0]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/tags/0.1.0
[Unreleased]: https://gitlab.com/lcg/neuro/v2/dataset/python-orm/compare?from=release&to=master

[Black]: https://pypi.org/project/black/
[Codecov]: https://codecov.io
[Coverage]: https://coverage.readthedocs.io
[Git hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
[Gitlab CI]: https://docs.gitlab.com/ee/ci
[Gitlab Pages]: https://docs.gitlab.com/ee/user/project/pages/
[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[PEP-517]: https://www.python.org/dev/peps/pep-0517/
[Poetry]: https://github.com/sdispater/poetry
[Poetry]: https://github.com/sdispater/poetry
[PyPI]: https://pypi.org
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
[Sphinx]: https://www.sphinx-doc.org/en/master/index.html
[TestPyPI]: https://test.pypi.org
[bumpversion]: https://github.com/peritus/bumpversion
[contents repository]: https://gitlab.com/lcg/neuro/v2/dataset/contents
[gitignore]: https://git-scm.com/docs/gitignore
[open-source]: https://opensource.org/
[post-setup-vs-requirements]: https://caremad.io/posts/2013/07/setup-vs-requirement/
[pre-commit]: https://pre-commit.com/
[pytest]: https://pytest.org/
[repository-codecov]: https://codecov.io/gl/lcg:neuro:v2:dataset/python-orm
[setup.py]: https://docs.python.org/3.6/distutils/setupscript.html

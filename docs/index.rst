======================================================================================
lcg-neuro-v2-dataset --- High resolution recordings in primate secondary visual cortex
======================================================================================

.. toctree::
   :maxdepth: 2

   Get started <README>
   env
   api/index
   cli
   CHANGELOG
   CONTRIBUTING
   MANIFEST

.. todo:: Include a minimal tutorial.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

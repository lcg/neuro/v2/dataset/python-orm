..
   vim: et: sw=3: ts=3: tw=120:
   vim: filetype=rst:
   vim: spell: spelllang=en:

=============================================
:mod:`v2_dataset.model` --- Dataset ORM model
=============================================

All entity classes and model supporting objects (such as enumerations and helper decorators) are available in the :mod:`v2_dataset` namespace.
However, they are implemented and documented into the following separate submodules:

.. contents::
    :local:

:mod:`v2_dataset.model.base` --- basic building blocks
======================================================

.. automodule:: v2_dataset.model.base
   :members:
   :show-inheritance:
   :undoc-members:

:mod:`v2_dataset.model.core` --- central dataset entity types
=============================================================

.. automodule:: v2_dataset.model.core
   :members:
   :show-inheritance:
   :undoc-members:

:mod:`v2_dataset.model.protocol` --- details of experimental protocols
======================================================================

.. automodule:: v2_dataset.model.protocol
   :members:
   :show-inheritance:
   :undoc-members:

:mod:`v2_dataset.model.sorting` --- entity types related to spike sorting
=========================================================================

:mod:`v2_dataset.model.sorting.core` --- arbitrary spike sorting
----------------------------------------------------------------

.. automodule:: v2_dataset.model.sorting.core
   :members:
   :show-inheritance:
   :undoc-members:

:mod:`v2_dataset.model.sorting.embedded` --- spike sorting embedded into PLX files (recordings)
-----------------------------------------------------------------------------------------------

.. automodule:: v2_dataset.model.sorting.embedded
   :members:
   :show-inheritance:
   :undoc-members:

:mod:`v2_dataset.model.stimulus` --- details of visual stimuli
==============================================================

.. automodule:: v2_dataset.model.stimulus
   :members:
   :show-inheritance:
   :undoc-members:

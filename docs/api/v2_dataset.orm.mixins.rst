=================================================================================================
:mod:`v2_dataset.orm.mixins` --- Convenience mixin base classes for declarative entity definition
=================================================================================================

.. automodule:: v2_dataset.orm.mixins
    :members:
    :show-inheritance:
    :undoc-members:

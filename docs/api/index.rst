..
   vim: et: sw=3: ts=3: tw=120:
   vim: filetype=rst:
   vim: spell: spelllang=en:

=============
API reference
=============

.. image:: schema.svg
   :alt: Schema of dataset's entity-relationship (ER) model.

.. toctree::
   :glob:

   *


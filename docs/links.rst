
.. _SQLAlchemy declarative base: https://docs.sqlalchemy.org/en/13/orm/extensions/declarative/index.html
.. _SQLAlchemy engine: https://docs.sqlalchemy.org/en/13/core/connections.html
.. _SQLAlchemy joined-table inheritance: https://docs.sqlalchemy.org/en/13/orm/inheritance.html
.. _SQLAlchemy scoped sessions: https://docs.sqlalchemy.org/en/13/orm/contextual.html
.. _SQLAlchemy session: https://docs.sqlalchemy.org/en/13/orm/session_basics.html
.. _SQLAlchemy: https://www.sqlalchemy.org/
.. _SQLite: https://www.sqlite.org/index.html
.. _contents repository: https://gitlab.com/lcg/neuro/v2/dataset/contents
.. _dataset: https://gitlab.com/lcg/neuro/v2/dataset/python-orm
.. _environ_config: https://pypi.org/project/environ-config/
.. _lcg-neuro-plx: https://lcg.gitlab.io/neuro/python-plx

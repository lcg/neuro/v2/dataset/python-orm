======================
Command-line interface
======================


:mod:`v2_dataset` tools
=======================

.. module:: v2_dataset.__main__

.. click:: v2_dataset.__main__:main
    :prog: v2_dataset
    :show-nested:


:mod:`v2_dataset.model` tools
=============================

.. module:: v2_dataset.model.__main__

.. click:: v2_dataset.model.__main__:main
    :prog: v2_dataset.model
    :show-nested:


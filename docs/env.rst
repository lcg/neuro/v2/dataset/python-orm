=========================
Environment configuration
=========================

Relevant environment variables:

.. csv-table::

    V2_DATASET_DIR,:attr:`v2_dataset.Config.dataset_dir`
    V2_DATASET_DB_AUTO_INIT,:attr:`v2_dataset.db.Config.auto_init`
    V2_DATASET_DB_ECHO,:attr:`v2_dataset.db.Config.echo`
    V2_DATASET_DB_PATH,:attr:`v2_dataset.db.Config.path`

"""Entity-relationship classes relative to spike sorting methods (:mod:`v2.sorting`).
"""

from .core import *
from .embedded import *

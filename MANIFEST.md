# Manifest

* [`.bumpversion.cfg`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/.bumpversion.cfg) &mdash; Configuration file for the [bumpversion] version-tagging package.
* [`.coveragerc`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/.coveragerc) &mdash; Configuration file for the [Coverage] reporting tool.
* [`.gitignore`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/.gitignore) &mdash; List of files and directories paths/patterns [ignored by Git][gitignore].
* [`.gitlab-ci.yml`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/.gitlab-ci.yml) &mdash; Continuous integration/deploy configuration ([GitLab CI]), configured for:
  * Running a [pytest]-based test suite and reporting results with [Codecov] at https://codecov.io/gl/lcg:neuro:v2:dataset/python-orm,
  * Building the [Sphinx]-based documentation and deploying it via [Gitlab Pages] to https://lcg.gitlab.io/neuro/v2/dataset/python-orm, and
  * Uploading successfully built, tagged distributions to [TestPyPI] (defaults to https://test.pypi.org/project/lcg-neuro-v2-dataset).
* [`.pre-commit-config.yaml`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/.pre-commit-config.yaml) &mdash; Configuration file for the [pre-commit] package, which aids in applying useful [Git Hooks] in team workflows. Includes:
  * Automatic code styling with [Black].
* [`CHANGELOG.md`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/CHANGELOG.md) &mdash; history of changes, which follows the [Keep a Changelog] standard.
* [`MANIFEST.md`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/MANIFEST.md) this very file.
* [`README.md`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/README.md) &mdash; repository front-page.
* [`dataset`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/dataset) &mdash; the [contents repository], merged in as a subtree.
* [`docs/`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/docs) &mdash; [Sphinx]-based documentation setup, which includes:
  * [`conf.py`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/docs/conf.py) &mdash; [Sphinx] configuration file,
  * [`docs/index.rst`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/docs/index.rst) &mdash; documentation master file, and
  * [`Makefile`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/docs/Makefile) for building the docs easily.
* [`poetry.lock`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/poetry.lock) &mdash; [Poetry]'s resolved dependency file. Whereas [`pyproject.toml`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/pyproject.toml) specifies [*as abstract as possible*][post-setup-vs-requirements] dependencies, this file 
* [`pyproject.toml`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/pyproject.toml) &mdash; [PEP-517]-compliant packaging metadata, configured with the [Poetry] system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic [setup.py] file found in *classical* Python packaging.
* [`src/v2_dataset`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/v2_dataset) &mdash; Base directory of the example Python package distributed by this repository.
* [`tests`](https://gitlab.com/lcg/neuro/v2/dataset/python-orm/blob/master/tests) &mdash; [pytest]-powered test-suite.


[Black]: https://pypi.org/project/black/
[Codecov]: https://codecov.io
[Coverage]: https://coverage.readthedocs.io
[Git hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
[Gitlab CI]: https://docs.gitlab.com/ee/ci
[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[PEP-517]: https://www.python.org/dev/peps/pep-0517/
[Poetry]: https://github.com/sdispater/poetry
[PyPI]: https://pypi.org
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
[Sphinx]: https://www.sphinx-doc.org/en/master/index.html
[TestPyPI]: https://test.pypi.org
[bumpversion]: https://github.com/peritus/bumpversion
[contents repository]: https://gitlab.com/lcg/neuro/v2/dataset/contents
[gitignore]: https://git-scm.com/docs/gitignore
[open-source]: https://opensource.org/
[post-setup-vs-requirements]: https://caremad.io/posts/2013/07/setup-vs-requirement/
[pre-commit]: https://pre-commit.com/
[pytest]: https://pytest.org/
[repository-codecov]: https://codecov.io/gl/lcg:neuro:v2:dataset/python-orm
[setup.py]: https://docs.python.org/3.6/distutils/setupscript.html
